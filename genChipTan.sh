#!/bin/bash

str=""
ncoord=$(python -c 'import codecs; print(codecs.encode(b"N50Gxx.xxxx","hex_codec"))'|sed 's/^..//'| sed 's/.$//')
ecoord=$(python -c 'import codecs; print(codecs.encode(b"E8Gxx.xxxx","hex_codec"))'|sed 's/^..//'| sed 's/.$//')

str=$str'00' # length =auto
str=$str'0' # bcd=yes
str=$str'4 ' #bytes lang
str=$str'855 ' # mask 8xx x=freitext
str=$str'49552 ' # foobar
str=$str'1' #cod = ascii
str=$str'b ' #len
str=$str$ncoord
str=$str' 1' #cod = ascii
str=$str'a ' #len
str=$str$ecoord
str=$str' 00' #checks=auto


echo $str | tr -d '\s'
